document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent (event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

// ApplyTag
function insertTag(tag){
	return function(content){
		return '<' + tag + '>' + content + '</' + tag + '>';
	}
}

const isParagraph = text => (!R.test(/^#/, text))
const isHeader = text => (R.test(/^#/, text))
const isOdd = num =>  (num % 2)

const matching = text => {
	if(isParagraph(text)){
		return R.compose(insertTag('p'))(text)
	}else if(isHeader(text)){
		const tag = insertTag('h'+findHeaderSize(text))
		return R.compose(tag)(R.replace(/#/g, '', text))
	}else{
		return text
	}
}

function findHeaderSize(header) {
	const isHeader = x => x === '#';
	return R.takeWhile(isHeader, header).length
}

function parseMarkDown (content) {
	const contentArr = R.split(/(?:\n\n)/gi, content)
	const recieved = R.map(matching, contentArr)
	return R.join('', R.map(toBold, recieved))
}

function toBold(text) {
	const splitStar = R.split('*', text)
	splitStar.map((a, b) => { 
		isOdd(b) ? splitStar[b] = R.compose(insertTag('b'))(a) : null
	})
	return R.join('',splitStar)
}